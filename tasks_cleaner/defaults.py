# encoding: utf-8
""" This module contains all default settings.

"""
from __future__ import absolute_import, division, print_function, unicode_literals

import sys

app_name = "tasks_cleaner"

config_root = 'global'

# GEmail defaults
tasks_cleaner_release = "0.0.1"
tasks_cleaner_environment = "dev"
# ---------------------------------------------
# Development environment variables
# ---------------------------------------------
tasks_cleaner_dev_logger_enable = "True"
tasks_cleaner_dev_logger_level = "DEBUG"

tasks_cleaner_dev_database_url = "host='localhost' dbname='gsked_ondemand' user='test' password='test'"
#tasks_cleaner_dev_database_url = 'postgresql://test:test@localhost/gsked_ondemand'
tasks_cleaner_dev_connect_timeout = "15"

# Clean variables
tasks_cleaner_dev_cleaner_pump_timer_cycle = "60"
tasks_cleaner_dev_cleaner_pump_delete_older_than_days = "1"
tasks_cleaner_dev_cleaner_pump_delete_from_create_date = "False"
tasks_cleaner_dev_cleaner_pump_delete_from_update_date = "True"
