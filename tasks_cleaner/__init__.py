# encoding: utf-8

import os
import click
import sys
import time
import logging

from . import defaults
from . import config
from . import tasks_cleaner

def get_logger_level_from_string(level):
    level = level.lower()
    if level == 'debug':
        return logging.DEBUG
    elif level == 'info':
        return logging.INFO
    elif level == 'warning':
        return logging.WARNING
    elif level == 'error':
        return logging.ERROR
    return logging.CRITICAL # 'Critical'

class InfoFilter(logging.Filter):
        def filter(self, rec):
            return rec.levelno in (logging.DEBUG, logging.INFO)

@click.group()
@click.option('--debug', default=False, is_flag=True)
@click.option('--logfile', type=str)
@click.option('--logfile-level', type=click.Choice(['Debug', 'Info', 'Warning', 'Error', 'Critical']))
@click.pass_context
def cli(context,debug,logfile,logfile_level):
    
    context.obj['DEBUG'] = debug if debug else False
    logFileLevel = get_logger_level_from_string(logfile_level) if logfile_level else logging.DEBUG
    
    # create logger
    logger = logging.getLogger('tasktracker-cleaner')
    logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
     
    if debug:
        h1 = logging.StreamHandler(sys.stdout)
        h1.setLevel(logging.DEBUG)
        h1.setFormatter(formatter)
        h1.addFilter(InfoFilter())
   
        h2 = logging.StreamHandler(sys.stderr)
        h2.setLevel(logging.WARNING)
        h2.setFormatter(formatter)

        logger.addHandler(h1)
        logger.addHandler(h2)
    else:
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(formatter)
        logger.addHandler(ch)

    if logfile:
        # create file handler
        fh = logging.FileHandler(logfile)
        fh.setLevel(logFileLevel)
        fh.setFormatter(formatter)
        logger.addHandler(fh)

    # Store logger in context
    context.obj['LOGGER'] = logger
    logger.info('Debug mode is %s' % ('on' if debug else 'off'))
    logger.info('Log To File mode is %s' % ('on' if logfile else 'off'))

@cli.command() 
@click.pass_context
def clean(context):
    logger = context.obj['LOGGER']
    logger.info("Starting clean completed tasks")
    tasks_cleaner.run()

main = cli(obj={})

if __name__ == '__main__':
    main(obj={})
