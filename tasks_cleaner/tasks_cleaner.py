# encoding: utf-8

import os
import sys
import logging
import json
import time
import logging
import inspect
import psycopg2
import requests

from Queue import Queue
from datetime import datetime
from datetime import timedelta
from dateutil.parser import parse

import config

# -----------------------------------
#           Logger 
# -----------------------------------
logger = logging.getLogger('tasktracker-cleaner.tasks_clean')
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

def get_logger_level_from_string(level):
    level = level.lower()
    if level == 'debug':
        return logging.DEBUG
    elif level == 'info':
        return logging.INFO
    elif level == 'warning':
        return logging.WARNING
    elif level == 'error':
        return logging.ERROR
    return logging.CRITICAL # 'Critical'

class InfoFilter(logging.Filter):
    def filter(self, rec):
        return rec.levelno in (logging.DEBUG, logging.INFO)

# Select environment (production or development)
environment = config.get('tasks_cleaner/environment')
if config.get('tasks_cleaner/%s/logger/enable' % environment) == "True":
    level = config.get('tasks_cleaner/%s/logger/level' % environment)
    h1 = logging.StreamHandler(sys.stdout)
    h1.setLevel(get_logger_level_from_string(level))
    h1.setLevel(logging.DEBUG)
    h1.setFormatter(formatter)
    h1.addFilter(InfoFilter())
    logger.addHandler(h1)

    h2 = logging.StreamHandler(sys.stderr)
    h2.setLevel(logging.WARNING)
    h2.setFormatter(formatter)
    logger.addHandler(h2)
else:
    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)
    ch.setFormatter(formatter)
    logger.addHandler(ch)

# -----------------------------------
#           Global variables
# -----------------------------------
tasks_cleaner_queue = Queue(maxsize=0)

def _delete_task_from_table(task_id):
    """
    Delete the given task 
    """
     # Select environment (production or development)
    environment = config.get('tasks_cleaner/environment')
    
    # Get database url
    connstr = config.get('tasks_cleaner/%s/database_url' % environment)
    
    if connstr is None:
        logger.error('_delete_task_from_table() Failed to obtain database information')
        return False
    else:
        
        if "connect_timeout" not in connstr:
            connect_timeout = config.get('tasks_cleaner/%s/connect_timeout' % environment)
            if connstr.startswith('postgresql://'):
                connstr = connstr + "?connect_timeout=%s" % connect_timeout
            else:
                connstr = connstr + " connect_timeout=%s" % connect_timeout

        sql = "DELETE FROM celery_schema.tasktracker WHERE id = '%s'" % (task_id)
        #logger.debug("_delete_task_from_table() sql = %s" % sql)

        conn = None
        try:
            conn = psycopg2.connect(connstr)
            cur = conn.cursor()
            cur.execute(sql)
            conn.commit()
            cur.close()
        except (Exception, psycopg2.DatabaseError, psycopg2.OperationalError) as error:
            logger.error('_delete_task_from_table() Failed to delete task from tasktracker table. %s' % error)
        finally:
            if conn is not None:
                conn.close()

def _get_tasktracker_tasks(status_list, date, from_create_at, from_update_at):
    
    if status_list and not isinstance(status_list, list):
        logger.error("_get_tasktracker_tasks() invalid parameter expecting a non empty list for status_list")
        return None
    
    # Select environment (production or development)
    environment = config.get('tasks_cleaner/environment')
    
    # Get database url
    connstr = config.get('tasks_cleaner/%s/database_url' % environment)

    response = None    
    if connstr is None:
        logger.error('_get_tasktracker_tasks() Failed to obtain database information')
    else:

        if "connect_timeout" not in connstr:
            connect_timeout = config.get('tasks_cleaner/%s/connect_timeout' % environment)
            if connstr.startswith('postgresql://'):
                connstr = connstr + "?connect_timeout=%s" % connect_timeout
            else:
                connstr = connstr + " connect_timeout=%s" % connect_timeout
        
        sql = "SELECT id,created_at,updated_at,celery_id,status"
        sql = sql + " from celery_schema.tasktracker"

        where = ""
        sz = len(status_list)
        count = 0
        for item in status_list:
            count += 1
            if count == 1:
                op = "( status = '%s' " % item
            else:
                op = " status = '%s' " % item
            if count < sz:
                op = op + " OR "
            else:
                op = op + ")"
            where = where + op

        if from_create_at:
            where = where + " AND created_at < '%s'" % date

        if from_update_at:
            where = where + " AND updated_at < '%s'" % date
    
        sql = sql + " WHERE " + where 
    
        #print (sql)
        
        conn = None
        try:
            conn = psycopg2.connect(connstr)
            cur = conn.cursor()
            cur.execute(sql)
            rows = cur.fetchall()
            cur.close()
            if rows:
                response = []
                for row in rows:
                    # Populate data
                    data = {}
                    idx = 0
                    for name in ['id','created_at','updated_at','celery_id','status']:
                        if isinstance(row[idx], datetime):
                            data[name] = str(row[idx])
                        else:
                            data[name] = row[idx]
                        idx+=1
                    response.append(data)
        except (Exception, psycopg2.DatabaseError, psycopg2.OperationalError) as error:
            logger.error('_get_tasktracker_tasks() Failed to read data base %s' % error)
        finally:
            if conn is not None:
                conn.close()
    return response

# -----------------------------------
#       Class definitions
# -----------------------------------
class TasksPurgeLoop():
    def __init__(self, purge_queue):
        self.purge_queue = purge_queue
        self.logger = logging.getLogger('tasks_cleaner.TasksPurgeLoop')
        self.logger.debug('TasksPurgeLoop Initialized!')
        
    def run(self):
        try:
            while not self.purge_queue.empty():
                msg = None
                # Get non blocking data
                try:
                    msg = self.purge_queue.get(False)
                except:
                    pass
                if msg:
                    # signal the task is done
                    self.purge_queue.task_done()
                    self.logger.debug('run() Deleting message %s' % msg['id'])
                    _delete_task_from_table(msg['id'])

        except Exception as exc:
            self.logger.error("run() Exception %s" % exc)
        self.logger.debug("run() Terminated")

# -----------------------------------
#      Job Process Functions
# -----------------------------------
def clean_tasktracker():
    global tasks_cleaner_queue
    
    logger.debug("clean_tasktracker() running...")

    # Get parameters from consul
    environment = config.get('tasks_cleaner/environment')  
    value = config.get('tasks_cleaner/%s/cleaner_pump/delete_older_than_days' % environment)        
    delete_from_create_date = config.get('tasks_cleaner/%s/cleaner_pump/delete_from_create_date' % environment)        
    delete_from_update_date = config.get('tasks_cleaner/%s/cleaner_pump/delete_from_update_date' % environment)        
    purge_older_than_days = int(float(value)) if value else 10 #Default 10 days

    dt = datetime.now() - timedelta(days=purge_older_than_days)
    logger.debug("Deleting all tasks older than %s" % dt)                        

    tasks = _get_tasktracker_tasks(['RUN','COMPLETED','SUCCESS', 'FAILURE','CANCELED'], dt, delete_from_create_date == "True", delete_from_update_date == "True")
    if tasks and len(tasks)>0:
        for t in tasks:
            tasks_cleaner_queue.put(t)

        # Call purge loop       
        purge_loop = TasksPurgeLoop(tasks_cleaner_queue)
        purge_loop.run()
    logger.debug("clean_tasktracker() done")

# -----------------------------------
#              Main 
# -----------------------------------
def run():
    try:
        environment = config.get('tasks_cleaner/environment')  
        value = config.get('tasks_cleaner/%s/cleaner_pump/timer_cycle' % environment)        
        timer_cycle = int(float(value)) if value else 60 # Default 60 seconds

        while True:
            # Start removing tasks
            clean_tasktracker()

            logger.debug("sleeping for %s seconds" % timer_cycle)

            # sleep x seconds
            time.sleep(timer_cycle)

    except Exception as e:
        logger.error("run() Exception %s" % e)
    except KeyboardInterrupt:
        pass

    logger.info("Tasks Cleaner finished")

if __name__ == '__main__':
    run()