# encoding: utf-8

""" Configuration values are accessed using this module.

Configuration values come from different sources, prioritized as
following (higher number = higher priority):

    1. Environment (typically only used for development)
    2. Cache
    3. Consul
    4. Defaults

All values can be overridden with environment variables. Environment
variable names must be the same as the overridden setting, but prefixed
with the app name: smtp_to -> GSKED_SMTP_TO.

In Consul, settings are in the "global" folder. In other words, create
"global/smtp_host" to set the real SMTP host.

"""

from __future__ import absolute_import, division, print_function, unicode_literals

import json
import os
import consulate

try:
    from . import defaults
except:
    import defaults

""" Notes

Scope precedence:
    1. Global
    2. Division
    3. Country
    4. Region
    5. Branch

Maybe use python-decouple as a base?
https://github.com/henriquebastos/python-decouple

"""

__all__ = ['ConsulConnectionError', 'KeyNotFoundError', 'asbool', 'get', 'keys', 'listen']

DEFAULT_TTL = 3600

APP_NAME = defaults.app_name if __name__ == "config" else __name__.split('.')[0]

BOOLEAN_FALSE_STATES = {'0': False, 'no': False, 'false': False, 'off': False, '': False}
BOOLEAN_TRUE_STATES = {'1': True, 'yes': True, 'true': True, 'on': True}
_BOOLEAN_STATES = dict(BOOLEAN_TRUE_STATES, **BOOLEAN_FALSE_STATES)

LOGGER = False

class KeyNotFoundError(KeyError):

    def __str__(self):
        return 'config key not found: %s' % self.args[0]


class ConsulConnectionError(IOError):
    pass


class FileData(object):
    def __init__(self, json_file):
        """ Create an object with a data attribute.

        Args:
            json_file (file object): with json content.

        Raises:
            ValueError
        """
        self.data = json.loads(json_file.read())

    def get(self, key):
        """ Get a value from the given key.

        Args:
            key (str): key used to get the value. Key can contain ``/`` to
                       define a path like structure, ex: ``/global/files/``
                       where the first ans last ``/`` are optionals
                       ``/global/files/`` is equal to ``global/files``.

        Returns:
            object: any type of object ex: str, dict, None, etc.
        """
        # Remove leading and trailing slash and get a list of keys in path, ex:
        # "/global/files/" become ["global", "files"]
        keys = key.strip('/').split('/')
        result = self.data if keys else None
        for k in keys:
            if isinstance(result, dict) and k in result:
                result = result[k]
            else:
                result = None
                break
        return result

    def find(self, prefix):
        """ Find all keys with the specified prefix.

        Args:
            prefix (str): the prefix to search with.

        Returns:
            dict: dictionary of matches.

        Example:
            FileData(file).find('b')
            {'baz': 'qux', 'bar': 'baz'}
        """
        keys = prefix.strip('/').split('/')
        keys, prefixes = keys[:-1], keys[-1:]
        if prefixes:
            prefix = prefixes[0]
        data = self.data
        if keys:
            data = self.get('/'.join(keys)) or {}

        result = {}
        for key, value in data.items():
            if key.startswith(prefix):
                key_ = '/'.join(keys + [key])
                if isinstance(value, dict):
                    key_ += '/'
                    result[key_] = None
                    for k, v in value.items():
                        result[key_ + k] = v
                else:
                    result[key_] = value
        return result


def _get_data_source():
    json_file = os.getenv(('%s_CONSUL_DATA' % APP_NAME).upper())
    if json_file is None:
        return _get_consul().kv
    else:
        with open(json_file) as raw:
            return FileData(raw)


def asbool(value):
    """ Coerce a value into a boolean.

    Based on ``ConfigParser.RawConfigParser.getboolean``. Returns
    ``True`` for "1", "yes", "true", and "on", ``False`` for "0", "no",
    "false", and "off", and raises a ``ValueError`` for anything else.

    """
    if not isinstance(value, str):
        value = str(value)

    if value.lower() not in _BOOLEAN_STATES:
        raise ValueError('Not a boolean: %s' % value)

    return _BOOLEAN_STATES[value.lower()]


def keys(key, cache=True, ttl=DEFAULT_TTL):
    """ Retrieve the set of keys found at a specific point in the hierarchy.

    Args:
        key (str): Config key for which to retrieve and return the value.
        cache (bool): Bypass read cache if True, if possible.
        ttl (int): Number of seconds before forcing a cache refresh.

    """
    keys = _get_data_source().find(key).keys()

    if not bool(keys):
        # Try to get the keys from defaults.py
        defaults_key = key.replace("/", "_").replace("-","_")
        data = getattr(defaults, defaults_key, '__NOT_FOUND__')
        if data != '__NOT_FOUND__':
            keys = list(data.keys())

    return {k.split('/')[-1] for k in keys}

def get(key, cache=True, ttl=DEFAULT_TTL, branch=None):
    """ Fetch a configuration value according to precedence rules.

    Args:
        key (str): Config key for which to retrieve and return the value.
        cache (bool): Bypass read cache if True, if possible.
        ttl (int): Number of seconds before forcing a cache refresh.
        branch (str): The name of the branch.

    Notes:
        Writes all return values to cache.  Uses the cache if `ttl` is
        expired or if a config source is unavailable for some reason.

    Raises:
        KeyNotFoundError: If `key` wasn't found and no default was
            provided.

    Returns:
        str: The value for the requested `key`.
    """
    # TODO: implement cache.
    data = _get_data_source()
   
    config_root = os.getenv('CONSUL_APP_CONFIG_ROOT')
    if config_root is None:
        raise EnvironmentError("Missing Environment Variable CONSUL_APP_CONFIG_ROOT")
        
    if config_root.endswith("/") == False:
        config_root = config_root + "/"

    #Fixing issue with environment vars
    prefix = APP_NAME + "/"
    if key.startswith(prefix):
        newKey = key.replace(prefix,"")
    else:
        newKey = key

    if branch is not None:
        name = '_'.join([APP_NAME, branch.replace('-', '_'), newKey])
        path = '/'.join(['branches', branch, key])
    else:
        name = '_'.join([APP_NAME, newKey])
        path =  config_root + key

    # Fix name replace characters
    name = name.replace("/", "_").replace("-","_")

    if LOGGER:
        print ("Name -----: %s" % name.upper())
        print ("Path -----: %s" % path)
    
    # Format the key to be able to find in inside the defaults.py
    defaults_key = key.replace("/", "_").replace("-","_")
    
    value = os.getenv(name.upper()) or data.get(path) or getattr(defaults, defaults_key, '__NOT_FOUND__')

    if value == '__NOT_FOUND__':
        raise KeyNotFoundError(key)

    return value
    

def listen(key, callback):
    """ Listen to configuration changes, and call a function on any change.

    Args:
        key (str): Config key for which to listen for changes.
        callback: Callable to call upon value changes.  It will be called
            with the new value as its only argument.

    Raises:
        KeyError: If `key` wasn't found.

    Returns:
        bool: True if the listener was successfully set up.
    """


class EmptyConsul(object):

    class kv(object):

        @classmethod
        def find(cls, key):
            return dict()

        @classmethod
        def get(cls, key):
            return None


def _get_consul(host=None, port=None):
    """ Return a connected Consul client.

    Note: The client is cached to save on the connection overhead, but
    no effort has been made to understand what concurrency issues there
    might exist because of this, whether in the context of Zato or
    otherwise.

    """
    if not hasattr(_get_consul, 'consul'):
        get_var = lambda name: (os.getenv(name) or '').strip() or None
        host = host or get_var('CONSUL_PORT_8500_TCP_ADDR')
        port = port or get_var('CONSUL_PORT_8500_TCP_PORT')
        if host:
            kwargs = dict(host=host)
            if port:
                kwargs['port'] = port
            _get_consul.consul = consulate.Consul(**kwargs)
            try:
                _get_consul.consul.agent.members()  # Trigger a connection.
            except Exception as e:
                raise ConsulConnectionError("Can't connect to Consul ({!r}).".format(e))

        else:
            _get_consul.consul = EmptyConsul

    return _get_consul.consul
