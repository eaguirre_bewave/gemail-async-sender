# encoding: utf-8
from __future__ import absolute_import, division, print_function, unicode_literals

import codecs
import sys

from setuptools import find_packages, setup

with codecs.open('README.md', encoding='utf-8') as f:
    long_description = f.read()

if 'PyPy' in sys.version:
    PSYCOPG = 'psycopg2cffi'
else:
    PSYCOPG = 'psycopg2-binary'

install_requires = [
    'click',
    'pip-tools',
    'requests',
    PSYCOPG,
    'consulate',
    'python-dateutil'
]

dev_requires = [
    # development tools
    'ipython',
]
test_requires = [
]

extras_require = {
    'test': test_requires,
    'dev': dev_requires + test_requires,
}

setup(
    name='tasks-cleaner',
    version='0.0.1',
    description='Tasks cleaner',
    long_description=long_description,
    author='The Garda Team',
    author_email='jean-francois.menard@garda.com',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Topic :: Office/Business :: Mailing',
        'License :: Other/Proprietary License',
        'Programming Language :: Python :: 2.7'
    ],
    keywords='Tasks',
    packages=find_packages(exclude=['contrib', 'docs', 'tests*']),
    install_requires=install_requires,
    extras_require=extras_require,
    entry_points={
        'console_scripts': [
            'tasks_cleaner=tasks_cleaner:main',
        ],
    },
)
